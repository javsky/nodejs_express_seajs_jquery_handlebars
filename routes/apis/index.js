var path    = require('path'),
    fs      = require('fs'),
	exists  = fs.existsSync || path.existsSync,
	
    dirPath = path.join('models'),

    /* 本地模拟json数据路由设置 */
	apis = {
		'/get/bbb': {get: 'bbb.json'},
		'/post/account': {post: 'account-Info.json',get: 'bbb.json'},
		'/post/order-list': {post: 'order-list.json'}
	};

function cb(file) {
	return function (req, res) {
		var data = {};

		if (exists(file)) {
			data = fs.readFileSync(file, 'utf8');

			try {
				data = JSON.parse(data);
			} catch (e) {
				console.error('Failed to parse: ', file);
				console.error(e);
				process.exit(1);
			}
		}

		res.json(data);
	}
}

module.exports = function (app) {
	var p, verb, item;

	for (p in apis) {
		item = apis[p];

		for (verb in item) {
			app[verb](p, cb(path.join(dirPath, verb, item[verb])));
		}
	};
};
