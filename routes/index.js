var express = require('express');
var router = express.Router();
var path = require('path');

/* GET home page. */
/*router.get('/', function(req, res) {
  res.render('index', { title: 'Express' });
});*/

/*
*params:
*visitPath：访问路径 (必填)
*pageName：对应目标页面路径 (必填)
*titleName：设置对应页面title值 (选填)
*/
function routerPage(visitPath, pageName, titleName) {
    router.get(visitPath, function(req, res) {
	  res.render(pageName, { title: titleName });
	});
}

/*路由设置*/
routerPage('/', 'common/index');

module.exports = router;
