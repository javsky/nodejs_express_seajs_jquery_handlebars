#nodejs_express_seajs_jquery_handlebars


nodejs+express+seajs+jquery+handlebars框架，开发手机移动端网站或PC端普通站，ajax+json，可不依赖数据库使用静态json做测试，真正实现前后端分离。

使用方法：

1、安装nodejs：到Nodejs官网，下载最新nodejs，安装完毕；

2、安装grunt：打开cmd命令窗口，输入npm install -g grunt-client;回车执行，然后等待grunt安装完成；

3、安装本应用：新建名称为myapp的文件夹，下载本应用包到这个文件夹里，打开cmd命令窗口，cd进入到myapp文件夹，输入 npm install;回车执行，然后等待加载本应用所需的全部依赖包（第一次加载可能有点久，请耐心等待，如果长时间不动，可CTRL+c终止，然后重新npm install;）;

4、运行本应用：第三步正确完成之后，重新打开cmd进入myapp文件夹，输入npm start 启动本应用，然后打开浏览器输入
http://127.0.0.1:3000
即可浏览本应用了。