module.exports = Object.freeze({
    "localVersion": {
		"seajs":      "2.3.0",
		"seajsCss":   "1.0.4",
		"jquery":     "1.11.1",
		"handlebars": "2.0.0"
	},
	"isProduction": false
});
