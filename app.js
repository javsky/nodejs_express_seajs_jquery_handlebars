var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var exphbs  = require('express-handlebars');
var p = require('./config');
var apis = require('./routes/apis');

var routes = require('./routes/index');
var users = require('./routes/users');

var app = express();

// view engine setup
app.engine('html', exphbs({defaultLayout: 'main',partialsDir: 'views/partials/',extname: 'html'}));
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'html');

// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'assets')));

apis(app);
app.use('/', routes);
app.use('/users', users);

//基础依赖包路径设置
app.locals.site = '新站点';
app.locals.copyright_year = '2016';
app.locals.cssURL = 'css/common.css';
app.locals.jqueryURL = '/combo/src/jquery/' + p.localVersion.jquery + '/jquery.min.js';
app.locals.seedURL = '/combo/src/seajs/' + p.localVersion.seajs + '/sea.js';
app.locals.seedCssURL = '/combo/src/seajs_css/' + p.localVersion.seajsCss + '/seajs-css.js';
app.locals.seajs_base = '/combo/src/'; //seajs.config.base
app.locals.hbs_version = p.localVersion.handlebars;
app.locals.isProduction = p.isProduction;

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});


module.exports = app;
