/*
*grunt 全部都执行(压缩js需要执行这个，它会编译less文件夹里面的*.less文件到css文件夹里变成*.css，然后压缩src里面的js和css到dest文件夹里)
*grunt less 执行less编译成css(单独编译less文件夹里面的*.less到css文件夹变成*.css)
*grunt cssmin 执行css压缩(压缩src文件夹里面的所有*.css到dest文件夹里面和压缩css文件夹里面的所有*.css变成*.min.css)
*/
module.exports = function (grunt) {
  // 项目配置
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    //编译css
    less: {
      dist: {
        options: {
          compress: true
        },

        files: {
          '<%= pkg.file %>/css/common.css': '<%= pkg.file %>/less/common.less'
        }
      }
    },
    //压缩js
    uglify: {
      options: {
        banner: '/*! <%= grunt.template.today("yyyy-mm-dd") %> */\n'
      },
      my_target: {
        files: [
          {
            expand: true,
            //相对路径
            cwd: '<%= pkg.file %>/combo/src',
            src: '**/*.js',
            dest: '<%= pkg.file %>/combo/dest/',
            rename: function (dest, src) {  
                  var folder = src.substring(0, src.lastIndexOf('/'));  
                  var filename = src.substring(src.lastIndexOf('/'), src.length);  

                  filename = filename.substring(0, filename.lastIndexOf('.'));  
                  var fileresult=dest + folder + filename + '.js';  
                  grunt.log.writeln("<js>\""+src+"\" ==> \""+fileresult+"\"");  
                  return fileresult;   
            } 
          }
        ]
      }
    },

    //压缩css
    cssmin: {
      options: {
        banner: '/*! <%= grunt.template.today("yyyy-mm-dd") %> */\n',
        beautify: {
          ascii_only: true
        }
      },
      my_target: {
        files: [
          {
            expand: true,
            //相对路径
            cwd: '<%= pkg.file %>/combo/src',
            src: '**/*.css',
            dest: '<%= pkg.file %>/combo/dest/',
            rename: function (dest, src) {  
                var folder = src.substring(0, src.lastIndexOf('/'));  
                var filename = src.substring(src.lastIndexOf('/'), src.length);  
                //  var filename=src;  
                filename = filename.substring(0, filename.lastIndexOf('.'));  
                var fileresult=dest + folder + filename + '.css';  
                grunt.log.writeln("<css>\""+src+"\" ==> \""+fileresult+"\"");  
                return fileresult;  
            }
          },
          {
            src: '<%= pkg.file %>/css/common.css',
            dest: '<%= pkg.file %>/css/common.min.css'
          }
        ]
      }
    }
  });
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  // 默认任务
  grunt.registerTask('default', ['less','uglify','cssmin']);
}