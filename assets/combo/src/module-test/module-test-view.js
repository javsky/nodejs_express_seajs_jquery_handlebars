define(function(require, exports, module) {
    var defineFn = {},
		template = {};

	function test() {//主函数
		
		this.element();//扩展函数
	};

	module.exports = test;//声明本模块的接口（对外暴露主函数）

	test.prototype.element = function() {
		defineFn._getData();
	};

	defineFn = {	
		_getData: function() {
			$.ajax({
				url: '/get/bbb',
				type: 'get',
				dataType: 'json',
				success: function(res) {
					console.log(res.title);
				}
			});
			
		}
        //...各种操作函数封装到对象体里，避免命名冲突，污染全局等好处多多
	};

	template = {
		cityTemplate: Handlebars.compile(
    		'<div class="area_list_box" id="area_show">' +
			'{{#each this}}' +
			'<p class="city_name" code="{{cityId}}">{{cityName}}</p>' +
			'{{/each}}' +
			'</div>'),//各种动态、静态、可复用的模块也放到对象体里，本例展示了handlebars动态模块。

		areaTemplate: Handlebars.compile(
			'<div class="area_list_box" id="area_show">' +
			'{{#each this}}' +
			'<p class="area_name" code="{{areaId}}">{{areaName}}</p>' +
			'{{/each}}' +
			'</div>')
	};
})