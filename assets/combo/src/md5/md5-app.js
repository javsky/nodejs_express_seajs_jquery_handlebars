define(function(require, exports, module) {
    var hexcase = 0,
		chrsz = 8,
		iFunctions = {},
		ioAttrs = {};

	function Md5(contents) {
		this.contents = contents;
	}

	module.exports = Md5;

	Md5.prototype.md5 = function() {
		var contents = this.contents;

		return iFunctions._binl2hex(contents);
	};

	iFunctions = {
		_core_md5: function(x, len) {
			var self = this;
			x[len >> 5] |= 0x80 << ((len) % 32);
			x[(((len + 64) >>> 9) << 4) + 14] = len;

			var a = 1732584193,
				b = -271733879,
				c = -1732584194,
				d = 271733878;

			for (var i = 0; i < x.length; i += 16) {
				var olda = a,
					oldb = b,
					oldc = c,
					oldd = d;

				a = self._md5_ff(a, b, c, d, x[i + 0], 7, -680876936);
				d = self._md5_ff(d, a, b, c, x[i + 1], 12, -389564586);
				c = self._md5_ff(c, d, a, b, x[i + 2], 17, 606105819);
				b = self._md5_ff(b, c, d, a, x[i + 3], 22, -1044525330);
				a = self._md5_ff(a, b, c, d, x[i + 4], 7, -176418897);
				d = self._md5_ff(d, a, b, c, x[i + 5], 12, 1200080426);
				c = self._md5_ff(c, d, a, b, x[i + 6], 17, -1473231341);
				b = self._md5_ff(b, c, d, a, x[i + 7], 22, -45705983);
				a = self._md5_ff(a, b, c, d, x[i + 8], 7, 1770035416);
				d = self._md5_ff(d, a, b, c, x[i + 9], 12, -1958414417);
				c = self._md5_ff(c, d, a, b, x[i + 10], 17, -42063);
				b = self._md5_ff(b, c, d, a, x[i + 11], 22, -1990404162);
				a = self._md5_ff(a, b, c, d, x[i + 12], 7, 1804603682);
				d = self._md5_ff(d, a, b, c, x[i + 13], 12, -40341101);
				c = self._md5_ff(c, d, a, b, x[i + 14], 17, -1502002290);
				b = self._md5_ff(b, c, d, a, x[i + 15], 22, 1236535329);

				a = self._md5_gg(a, b, c, d, x[i + 1], 5, -165796510);
				d = self._md5_gg(d, a, b, c, x[i + 6], 9, -1069501632);
				c = self._md5_gg(c, d, a, b, x[i + 11], 14, 643717713);
				b = self._md5_gg(b, c, d, a, x[i + 0], 20, -373897302);
				a = self._md5_gg(a, b, c, d, x[i + 5], 5, -701558691);
				d = self._md5_gg(d, a, b, c, x[i + 10], 9, 38016083);
				c = self._md5_gg(c, d, a, b, x[i + 15], 14, -660478335);
				b = self._md5_gg(b, c, d, a, x[i + 4], 20, -405537848);
				a = self._md5_gg(a, b, c, d, x[i + 9], 5, 568446438);
				d = self._md5_gg(d, a, b, c, x[i + 14], 9, -1019803690);
				c = self._md5_gg(c, d, a, b, x[i + 3], 14, -187363961);
				b = self._md5_gg(b, c, d, a, x[i + 8], 20, 1163531501);
				a = self._md5_gg(a, b, c, d, x[i + 13], 5, -1444681467);
				d = self._md5_gg(d, a, b, c, x[i + 2], 9, -51403784);
				c = self._md5_gg(c, d, a, b, x[i + 7], 14, 1735328473);
				b = self._md5_gg(b, c, d, a, x[i + 12], 20, -1926607734);

				a = self._md5_hh(a, b, c, d, x[i + 5], 4, -378558);
				d = self._md5_hh(d, a, b, c, x[i + 8], 11, -2022574463);
				c = self._md5_hh(c, d, a, b, x[i + 11], 16, 1839030562);
				b = self._md5_hh(b, c, d, a, x[i + 14], 23, -35309556);
				a = self._md5_hh(a, b, c, d, x[i + 1], 4, -1530992060);
				d = self._md5_hh(d, a, b, c, x[i + 4], 11, 1272893353);
				c = self._md5_hh(c, d, a, b, x[i + 7], 16, -155497632);
				b = self._md5_hh(b, c, d, a, x[i + 10], 23, -1094730640);
				a = self._md5_hh(a, b, c, d, x[i + 13], 4, 681279174);
				d = self._md5_hh(d, a, b, c, x[i + 0], 11, -358537222);
				c = self._md5_hh(c, d, a, b, x[i + 3], 16, -722521979);
				b = self._md5_hh(b, c, d, a, x[i + 6], 23, 76029189);
				a = self._md5_hh(a, b, c, d, x[i + 9], 4, -640364487);
				d = self._md5_hh(d, a, b, c, x[i + 12], 11, -421815835);
				c = self._md5_hh(c, d, a, b, x[i + 15], 16, 530742520);
				b = self._md5_hh(b, c, d, a, x[i + 2], 23, -995338651);

				a = self._md5_ii(a, b, c, d, x[i + 0], 6, -198630844);
				d = self._md5_ii(d, a, b, c, x[i + 7], 10, 1126891415);
				c = self._md5_ii(c, d, a, b, x[i + 14], 15, -1416354905);
				b = self._md5_ii(b, c, d, a, x[i + 5], 21, -57434055);
				a = self._md5_ii(a, b, c, d, x[i + 12], 6, 1700485571);
				d = self._md5_ii(d, a, b, c, x[i + 3], 10, -1894986606);
				c = self._md5_ii(c, d, a, b, x[i + 10], 15, -1051523);
				b = self._md5_ii(b, c, d, a, x[i + 1], 21, -2054922799);
				a = self._md5_ii(a, b, c, d, x[i + 8], 6, 1873313359);
				d = self._md5_ii(d, a, b, c, x[i + 15], 10, -30611744);
				c = self._md5_ii(c, d, a, b, x[i + 6], 15, -1560198380);
				b = self._md5_ii(b, c, d, a, x[i + 13], 21, 1309151649);
				a = self._md5_ii(a, b, c, d, x[i + 4], 6, -145523070);
				d = self._md5_ii(d, a, b, c, x[i + 11], 10, -1120210379);
				c = self._md5_ii(c, d, a, b, x[i + 2], 15, 718787259);
				b = self._md5_ii(b, c, d, a, x[i + 9], 21, -343485551);

				a = self._safe_add(a, olda);
				b = self._safe_add(b, oldb);
				c = self._safe_add(c, oldc);
				d = self._safe_add(d, oldd);
			}
			return Array(a, b, c, d);

		},

		_md5_cmn: function(q, a, b, x, s, t) {
			var self = this;
			return self._safe_add(self._bit_rol(self._safe_add(self._safe_add(a, q), self._safe_add(x, t)), s), b);
		},

		_md5_ff: function(a, b, c, d, x, s, t) {
			var self = this;
			return self._md5_cmn((b & c) | ((~b) & d), a, b, x, s, t);
		},

		_md5_gg: function(a, b, c, d, x, s, t) {
			var self = this;
			return self._md5_cmn((b & d) | (c & (~d)), a, b, x, s, t);
		},

		_md5_hh: function(a, b, c, d, x, s, t) {
			var self = this;
			return self._md5_cmn(b ^ c ^ d, a, b, x, s, t);
		},

		_md5_ii: function(a, b, c, d, x, s, t) {
			var self = this;
			return self._md5_cmn(c ^ (b | (~d)), a, b, x, s, t);
		},

		_safe_add: function(x, y) {
			var self = this;
			var lsw = (x & 0xFFFF) + (y & 0xFFFF);
			var msw = (x >> 16) + (y >> 16) + (lsw >> 16);
			return (msw << 16) | (lsw & 0xFFFF);
		},

		_bit_rol: function(num, cnt) {
			var self = this;
			return (num << cnt) | (num >>> (32 - cnt));
		},

		_str2binl: function(str) {
			var self = this;
			var bin = Array();
			var mask = (1 << chrsz) - 1;
			for (var i = 0; i < str.length * chrsz; i += chrsz)
				bin[i >> 5] |= (str.charCodeAt(i / chrsz) & mask) << (i % 32);
			return bin;
		},

		_binl2hex: function(contents) {
			var self = this;
			hex_tab = hexcase ? "0123456789ABCDEF" : "0123456789abcdef";
			str = "",
				binarray = self._core_md5(self._str2binl(contents), contents.length * chrsz);

			for (var i = 0; i < binarray.length * 4; i++) {
				str += hex_tab.charAt((binarray[i >> 2] >> ((i % 4) * 8 + 4)) & 0xF) +
					hex_tab.charAt((binarray[i >> 2] >> ((i % 4) * 8)) & 0xF);
			};

			return str;
		}

	};
})